from io import StringIO
import unittest
import urllib
import cherrypy
import os
import sys
import json
import socket
import base64
import hashlib
import subprocess
from cryptography.fernet import Fernet
from nose.plugins.attrib import attr

dir_lib = os.path.dirname(os.path.realpath(__file__)) + '/../../lib'
sys.path.append(dir_lib)

import eva.sysapi
import eva.uc.ucapi
import eva.core
import eva.traphandler
import eva.udpapi
import eva.notify
import eva.uc.ucapi
import eva.apikey
import eva.uc.controller
import eva.uc.ucapi
import eva.logs
import eva.runner
import eva.wsapi
from .sys_test import fmt

local = cherrypy.lib.httputil.Host("127.0.0.1", 50000, "")
remote = cherrypy.lib.httputil.Host("127.0.0.1", 50001, "")
product_build = 2018073001
product_code = 'uc'

setup_mode = False
k = "k=eva&"

def setUpModule():
    cherrypy.config.update({'environment': "test_suite"})
    cherrypy.server.unsubscribe()
    eva.core.init()
    eva.core.set_product(product_code, product_build)
    eva.core.product_name = 'EVA Universal Controller'
    cfg = eva.core.load(initial=True)
    eva.traphandler.update_config(cfg)
    eva.udpapi.update_config(cfg)    
    eva.apikey.load()
    eva.traphandler.start()
    eva.uc.ucapi.start()
    eva.uc.controller.start()
    eva.notify.init()
    eva.notify.load()
    eva.notify.start()
    cherrypy.tree.mount(eva.uc.ucapi.UC_HTTP_API(), "/")
    cherrypy.engine.start()

setup_module = setUpModule

def tearDownModule():
    eva.notify.stop()
    eva.uc.controller.stop()
    cherrypy.engine.exit()
teardown_module = tearDownModule

class BaseCherryPyTestCase(unittest.TestCase):
    key = k
    unit1_id = "test_unit1"
    unit2_id = "test_unit2"
    unit3_id = "test_unit3"
    sensor1_id = "test_sensor1"
    group1 = "test_group1"
    group2 = "test_group2"
    group3 = "test_group3"
    device_group = "test_device5"
    groups = [group1, group2, group3, device_group]

    def webapp_request(self, path="/", method="GET", key_param=None):
        headers = [("Host", "127.0.0.1")]
        app = cherrypy.tree.apps['']
        request, response = app.get_serving(local, remote, "http", "HTTP/1.1")
        response = request.run(method, path, key_param, 'HTTP/1.1', headers, None)

        if response.status.startswith("500"):
            print(response.body)
            raise AssertionError("500 Unexpected error")
        return response

class TestUCAPI(BaseCherryPyTestCase):
    def setUp(self):
        global setup_mode, k
        if os.getenv('SETUP_MODE', False):
            setup_mode = True
            k = None
            eva.core.setup_on()

    def tearDown(self):
        for i in self.groups:
            response = self.webapp_request(path="/destroy",
                                            key_param=fmt.format("{0}g={1}",
                                            self.key, i))
            self.assertEqual(response.status, "200 OK")

    def create_unit(self, unit_id, group_id):
        response = self.webapp_request(path="/create_unit",
                                       key_param=fmt.format("{0}i={1}&g={2}",
                                       self.key, unit_id, group_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

    def create_sensor(self, sensor_id, group_id):
        response = self.webapp_request(path="/create_sensor",
                                       key_param=fmt.format("{0}i={1}&g={2}",
                                       self.key, sensor_id, group_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

    def check_forbidden(self, path):
        if not setup_mode:
            response = self.webapp_request(path=path, key_param="k=wrong_key")
            self.assertEqual(response.status, "403 API Forbidden")

    def test_test(self):
        self.check_forbidden("/test")

        response = self.webapp_request("/test", key_param=fmt.format("{0}",
                                       self.key))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        for i in ["debug", "polldelay", "product_build",
                  "product_code", "product_name", "result", "system",
                  "time", "uptime", "version", "acl"]:
            self.assertIn(i, response)
        if not setup_mode:
            self.assertEqual(response["acl"]["key_id"], "masterkey")
            self.assertEqual(response["acl"]["master"], True)

        response = self.webapp_request("/info")
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

    def test_state_history(self):
        self.check_forbidden("/state_history")

        # create unit
        self.create_unit(self.unit1_id, self.group1)

        # check unit state
        response = self.webapp_request(path="/state_history",
                                       key_param=fmt.format("{0}i={1}&s={2}&x={3}&t={4}",
                                       self.key,
                                       self.unit1_id, "20101216T172215", 1, "iso"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        for i in ["status", "t", "value"]:
            self.assertIn(i, response)


    def test_create_unit(self):
        self.check_forbidden("/state")

        # create first unit
        self.create_unit(self.unit1_id, self.group1)

        # check unit state
        response = self.webapp_request(path="/state",
                                       key_param=fmt.format("{0}i={1}&g={2}&p={3}&full={4}",
                                       self.key, self.unit1_id, self.group1,
                                       "U", 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        for i in ["action_enabled", "full_id", "group",
                  "id", "nstatus", "nvalue", "oid",
                  "status", "type", "value"]:
            self.assertIn(i, response)
        self.assertEqual(response["action_enabled"], False)
        self.assertEqual(response["full_id"], "{}/{}".format(self.group1,
                                                             self.unit1_id))
        self.assertEqual(response["group"], self.group1)
        self.assertEqual(response["id"], self.unit1_id)
        self.assertEqual(response["nstatus"], 0)
        self.assertEqual(response["nvalue"], "null")
        self.assertEqual(response["oid"], "unit:{}/{}".format(self.group1,
                                                              self.unit1_id))
        self.assertEqual(response["status"], 0)
        self.assertEqual(response["type"], "unit")
        self.assertEqual(response["value"], "null")
        self.assertEqual(response["config_changed"], True)
        self.assertEqual(response["description"], "")
        self.assertEqual(response["loc_x"], None)
        self.assertEqual(response["loc_y"], None)
        self.assertEqual(response["loc_x"], None)
        self.assertEqual(response["location"], "")
        self.assertEqual(response["status_labels"], [{'label': 'OFF', 'status': 0}, {'label': 'ON', 'status': 1}])


        # create second unit
        self.create_unit(self.unit2_id, self.group1)

        # check sorted units state
        response = self.webapp_request(path="/state",
                                       key_param=fmt.format("{0}g={1}&p={2}",
                                       self.key,
                                       self.group1, "U"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 2)
        self.assertEqual(response[0]["id"], self.unit1_id)
        self.assertEqual(response[1]["id"], self.unit2_id)

        # update item status ond/or value
        self.check_forbidden("/update")

        response = self.webapp_request(path="/update",
                                        key_param=fmt.format("{0}i={1}&s={2}&v={3}",
                                        self.key, self.unit1_id, 4, "new_value"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        response = self.webapp_request(path="/state",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["status"], 4)
        self.assertEqual(response["value"], "new_value")

        # create virtual unit and save its configuration on disk
        response = self.webapp_request(path="/create_unit",
                                       key_param=fmt.format("{0}i={1}&g={2}&virtual={3}&save={4}",
                                       self.key, "unit_id2", self.group1, 1, 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        # try to create unit with the same id
        response = self.webapp_request(path="/create_unit",
                                       key_param=fmt.format("{0}i={1}&g={2}",
                                        self.key, self.unit1_id, self.group1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'ERROR'})

    def test_view_groups_list(self):
        self.check_forbidden("/groups")

        self.create_unit(self.unit1_id, self.group1)
        self.create_unit(self.unit2_id, self.group2)

        response = self.webapp_request(path="/groups",
                                       key_param=fmt.format("{0}p=U",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertIn(self.group1, response)

        # check both groups are present
        response = self.webapp_request(path="/groups",
                                       key_param=fmt.format("{0}p=U",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(len(response), 2)

    def test_view_items_list(self):
        self.check_forbidden("/list")

        self.create_unit(self.unit1_id, self.group1)
        self.create_unit(self.unit2_id, self.group1)
        self.create_unit(self.unit3_id, self.group2)

        # list by item type
        response = self.webapp_request(path="/list",
                                       key_param=fmt.format("{0}p=U",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 3)
        self.assertEqual(response[0]["full_id"], "{}/{}".format(self.group1, self.unit1_id))
        self.assertEqual(response[1]["full_id"], "{}/{}".format(self.group1, self.unit2_id))

        # list by group
        response = self.webapp_request(path="/list",
                                       key_param=fmt.format("{0}g={1}",
                                       self.key, self.group2))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0]["full_id"], "{}/{}".format(self.group2, self.unit3_id))

    def test_unit_config(self):
        self.create_unit(self.unit1_id, self.group1)

        self.check_forbidden("/get_config")

        response = self.webapp_request(path="/get_config",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["full_id"], "{}/{}".format(self.group1,
                                                             self.unit1_id))
        self.assertEqual(response["id"], self.unit1_id)
        self.assertEqual(response["oid"], "unit:{}/{}".format(self.group1,
                                                              self.unit1_id))
        self.assertEqual(response["type"], "unit")

        # test save config
        self.check_forbidden("/save_config")

        response = self.webapp_request(path="/save_config",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        # test all configurable props
        self.check_forbidden("/list_props")

        response = self.webapp_request(path="/list_props",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        for i in ["action_allow_termination", "action_always_exec",
                  "action_driver_config", "action_enabled", "action_exec",
                  "action_queue", "action_timeout", "auto_off", "description",
                  "expires", "location", "mqtt_control", "mqtt_update",
                  "snmp_trap", "status_labels", "term_kill_interval",
                  "update_delay", "update_driver_config", "update_exec",
                  "update_exec_after_action", "update_if_action", "update_interval", 
                  "update_state_after_action", "update_timeout", "virtual"]:
            self.assertIn(i, response)

        # test set property
        self.check_forbidden("/set_prop")

        for i in ["action_allow_termination", "action_always_exec",
                  "action_enabled", "update_exec_after_action",
                  "update_if_action", "virtual", "update_exec",
                  "update_exec_after_action", "update_if_action",
                  "update_state_after_action", "virtual"]:
            response = self.webapp_request(path="/set_prop",
                                           key_param=fmt.format("{0}&i={1}&p={2}&v={3}",
                                           self.key, self.unit1_id, i,
                                           True))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {'result': 'OK'})

        for i in ["action_timeout", "location", "term_kill_interval",
                  "update_delay", "update_interval", "update_timeout", "action_queue"]:
            response = self.webapp_request(path="/set_prop",
                                           key_param=fmt.format("{0}i={1}&p={2}&v={3}",
                                           self.key, self.unit1_id, i,
                                           1))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {'result': 'OK'})


    def test_actions(self):
        # wrong key
        self.check_forbidden("/action")

        self.create_unit(self.unit1_id, self.group1)
        self.create_unit(self.unit2_id, self.group2)

        for i in [self.unit1_id, self.unit2_id]:
            response = self.webapp_request(path="/set_prop",
                                           key_param=fmt.format("{0}i={1}&p={2}&v={3}",
                                           self.key, i, "action_enabled",
                                           True))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {'result': 'OK'})

        response = self.webapp_request(path="/action",
                                       key_param=fmt.format("{0}i={1}&s={2}&v={3}&p={4}&u={5}&w={6}&q={7}",
                                       self.key, self.unit2_id, 3,
                                       "new_value", 80, "unique_id1", 5, 10))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["nstatus"], 3)
        self.assertEqual(response["nvalue"], "new_value")
        self.assertEqual(response["priority"], 80)
        self.assertEqual(response["uuid"], "unique_id1")

        # test action toggle
        self.check_forbidden("/action_toggle")

        response = self.webapp_request(path="/action_toggle",
                                       key_param=fmt.format("{0}i={1}&p={2}&u={3}",
                                       self.key, self.unit1_id, 80, "unique_id2"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["priority"], 80)
        self.assertEqual(response["uuid"], "unique_id2")
        self.assertEqual(response["nstatus"], 1)

        #TODO toggle logic
        response = self.webapp_request(path="/action_toggle",
                               key_param=fmt.format("{0}i={1}&p={2}&u={3}",
                               self.key, self.unit1_id,
                               60, "unique_id3"))
        response = json.loads(response.collapse_body())
        self.assertEqual(response["priority"], 60)
        self.assertEqual(response["uuid"], "unique_id3")
        # self.assertEqual(response['nstatus'], 0)

        # check last action
        response = self.webapp_request(path="/result",
                                       key_param=fmt.format("{0}&i={1}&u={2}",
                                       self.key, self.unit1_id, "unique_id3"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["uuid"], "unique_id3")
        self.assertEqual(response["item_id"], self.unit1_id)
        self.assertEqual(response["priority"], 60)

        # check unit1 actions
        response = self.webapp_request(path="/result",
                                       key_param=fmt.format("{0}&i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 2)

        for n, i in enumerate(["unique_id2", "unique_id3"]):
            self.assertEqual(response[n]["uuid"], i)

        # filter result by group
        response = self.webapp_request(path="/result",
                                       key_param=fmt.format("{0}g={1}",
                                       self.key, self.group2))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 1)
        self.assertEqual(response[0]["item_id"], self.unit2_id)
        self.assertEqual(response[0]["item_group"], self.group2)

        # disable actions
        self.check_forbidden("/disable_actions")

        response = self.webapp_request(path="/disable_actions",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        # enable actions
        self.check_forbidden("/enable_actions")

        response = self.webapp_request(path="/enable_actions",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        # terminate action
        response = self.webapp_request(path="/set_prop",
                                       key_param=fmt.format("{0}i={1}&p={2}&v={3}",
                                       self.key, self.unit1_id, "action_allow_termination",
                                       True))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        self.check_forbidden("/terminate")

        # response = self.webapp_request(path="/terminate",
        #                                key_param=fmt.format("{0}&u={1}&i={2}",
        #                                self.key, "unique_id3", self.unit1_id))
        # self.assertEqual(response.status, "200 OK")

        # test actions queue clean
        self.check_forbidden("/q_clean")

        response = self.webapp_request(path="/q_clean",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        # test actions queue kill
        self.check_forbidden("/kill")

        response = self.webapp_request(path="/kill",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, self.unit1_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

    def test_create_sensor(self):
        self.check_forbidden("/create_sensor")

        response = self.webapp_request(path="/create_sensor",
                                       key_param=fmt.format("{0}i={1}&g={2}",
                                       self.key, "sensor_id", self.group1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        # check sensor state
        response = self.webapp_request(path="/state",
                                       key_param=fmt.format("{0}i={1}&g={2}&p={3}",
                                       self.key, "sensor_id", self.group1, "S"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        for i in ["full_id", "group", "id", "oid", "status", "type", "value"]:
            self.assertIn(i, response)
        self.assertEqual(response["full_id"], "{}/{}".format(self.group1,
                                                             "sensor_id"))
        self.assertEqual(response["group"], self.group1)
        self.assertEqual(response["id"], "sensor_id")
        self.assertEqual(response["oid"], "sensor:{}/{}".format(self.group1,
                                                              "sensor_id"))
        self.assertEqual(response["status"], 0)
        self.assertEqual(response["type"], "sensor")
        self.assertEqual(response["value"], "null")

        # create virtual sensor and save its configuration on disk
        response = self.webapp_request(path="/create_sensor",
                                       key_param=fmt.format("{0}i={1}&g={2}&virtual={3}&save={4}",
                                       self.key, "sensor_id2", self.group1, 1, 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

    def test_create_multiupdate(self):
        self.check_forbidden("/create_mu")

        response = self.webapp_request(path="/create_mu",
                                       key_param=fmt.format("{0}i={1}&g={2}",
                                       self.key, "mu_id", self.group1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        # check mu state
        response = self.webapp_request(path="/state",
                                       key_param=fmt.format("{0}i={1}&g={2}&p={3}",
                                       self.key, "mu_id", self.group1, "M"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        for i in ["full_id", "group", "id", "oid", "status", "type", "value"]:
            self.assertIn(i, response)
        self.assertEqual(response["full_id"], "{}/{}".format(self.group1,
                                                             "mu_id"))
        self.assertEqual(response["group"], self.group1)
        self.assertEqual(response["id"], "mu_id")
        self.assertEqual(response["oid"], "mu:{}/{}".format(self.group1,
                                                              "mu_id"))
        self.assertEqual(response["status"], 0)
        self.assertEqual(response["type"], "mu")
        self.assertEqual(response["value"], "null")

        # create virtual mu and save its configuration on disk
        response = self.webapp_request(path="/create_mu",
                                       key_param=fmt.format("{0}i={1}&g={2}&virtual={3}&save={4}",
                                       self.key, "sensor_id2", self.group1, 1, 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

    def test_clone_items(self):
        for k, v in {"/create_unit": "unit1_id",
                     "/create_sensor": "sensor1_id"}.items():

            response = self.webapp_request(path=k,
                                           key_param=fmt.format("{0}i={1}&g={2}",
                                           self.key, v, self.group1))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {'result': 'OK'})

            self.check_forbidden("/clone")

            response = self.webapp_request(path="/clone",
                                           key_param=fmt.format("{0}i={1}&n={2}&g={3}",
                                           self.key, v, "new_" + v, self.group2))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {'result': 'OK'})

        # list units by item type
        response = self.webapp_request(path="/list",
                                       key_param=fmt.format("{0}p=U",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 2)
        self.assertEqual(response[0]["full_id"], "{}/{}".format(self.group1, "unit1_id"))
        self.assertEqual(response[1]["full_id"], "{}/{}".format(self.group2, "new_unit1_id"))

        # list sensors by item type
        response = self.webapp_request(path="/list",
                                       key_param=fmt.format("{0}p=S",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(len(response), 2)
        self.assertEqual(response[0]["full_id"],
                                     "{}/{}".format(self.group1,
                                                    "sensor1_id"))
        self.assertEqual(response[1]["full_id"],
                                     "{}/{}".format(self.group2,
                                                    "new_sensor1_id"))

        # list all units by group
        response = self.webapp_request(path="/list",
                                       key_param=fmt.format("{0}g={1}",
                                       self.key, self.group2))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(len(response), 2)
        self.assertEqual(response[0]["full_id"], "{}/{}".format(self.group2,
                                                                "new_sensor1_id"))
        self.assertEqual(response[1]["full_id"], "{}/{}".format(self.group2,
                                                                "new_unit1_id"))

        # clone group
        self.check_forbidden("/clone_group")

        response = self.webapp_request(path="/clone_group",
                                       key_param=fmt.format("{0}g={1}&n={2}&p={3}&r={4}",
                                       self.key, self.group2, self.group3, "new", "very_new"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        # list new group
        response = self.webapp_request(path="/list",
                                       key_param=fmt.format("{0}g={1}",
                                       self.key, self.group3))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(len(response), 2)
        self.assertEqual(response[0]["full_id"], "{}/{}".format(self.group3,
                                                                "very_new_sensor1_id"))
        self.assertEqual(response[1]["full_id"], "{}/{}".format(self.group3,
                                                                "very_new_unit1_id"))

    def test_device(self):
        file_name = "test_template.json"
        file_path = os.path.dirname(os.path.realpath(__file__)) + \
                                    "/../../runtime/tpl/" + file_name
        f_body = json.dumps({                                                                                                                                                                                                                                                                               
                                "sensors": [                                                                                                                                                                                                                                                                
                                    {                                                                                                                                                                                                                                                                       
                                        "group": "test_device{{ ID }}",                                                                                                                                                                                                                                                     
                                        "id": "test_device{{ ID }}.test_sensor1"                                                                                                                                                                                                                                                          
                                    }                                                                                                                                                                                                                                                                       
                                ],                                                                                                                                                                                                                                                                          
                                "units": [                                                                                                                                                                                                                                                                  
                                    {                                                                                                                                                                                                                                                                       
                                        "group": "test_device{{ ID }}",                                                                                                                                                                                                                                                     
                                        "id": "test_device{{ ID }}.test_untit1"                                                                                                                                                                                                                                                          
                                    }                                                                                                                                                                                                                                                                       
                                ]
                            })

        with open(file_path, "w+") as file:
            file.write(f_body)

        self.create_unit("test_device1." + self.unit1_id, self.device_group)
        self.create_sensor("test_device1." + self.sensor1_id, self.device_group)

        self.check_forbidden("/create_device")

        response = self.webapp_request(path="/create_device",
                                       key_param=fmt.format("{0}c={1}&t={2}",
                                       self.key, "ID=5", "test_template"))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        self.check_forbidden("/update_device")

        response = self.webapp_request(path="/update_device",
                                       key_param=fmt.format("{0}c={1}&t={2}",
                                       self.key, "ID=5", "test_template"))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        self.check_forbidden("/destroy_device")

        response = self.webapp_request(path="/destroy_device",
                                       key_param=fmt.format("{0}c={1}&t={2}",
                                       self.key, "ID=6", "test_template"))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {'result': 'OK'})

        try:
            os.remove(file_path)
        except:
            pass

    @attr('udp')
    @unittest.skipUnless(os.getenv('UDP_TEST'), "check fo 404")
    def test_udp_request(self):
        unit_status_1 = 5
        unit_status_2 = 8
        unit_value = 'test_unit_value'
        k_id = 'key1'

        bashCommand = 'bin/uc-cmd create unit:{0}/{1}'.format(self.group1, self.unit1_id)
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()

        bashCommand = 'bin/uc-cmd create unit:{0}/{1}'.format(self.group2, self.unit2_id)
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()

        # success test simple command
        command = '{0} u {1}'.format(self.unit1_id, unit_status_1)
        udp_request(command)

        result = bash_state(self.unit1_id)
        self.assertEquals(int(result['status']), unit_status_1)

        # success test multiple commands
        command = '|{0} u {1}|{2} u {3}|'.format(self.unit1_id, unit_status_2,
                                                 self.unit2_id,unit_status_1)
        udp_request(command)

        result = bash_state(self.unit1_id)
        self.assertEquals(int(result['status']), unit_status_2)

        result = bash_state(self.unit2_id)
        self.assertEquals(int(result['status']), unit_status_1)

        # success test multiple encrypted commands
        data = '{0} u {1} {2}|{3} u {4}'.format(self.unit1_id,
                                                unit_status_1, unit_value,
                                                self.unit2_id, unit_status_2)
        k = eva.apikey.key_by_id(k_id)
        _k = base64.b64encode(hashlib.sha256(k.encode()).digest())
        ce = Fernet(_k)
        encr_data = ce.encrypt(data.encode()).decode()
        command = '|{}|{}|'.format(k_id, encr_data)
        udp_request(command)

        result = bash_state(self.unit1_id)
        self.assertEquals(int(result['status']), unit_status_1)
        self.assertEquals(result['value'], unit_value)

        result = bash_state(self.unit2_id)
        self.assertEquals(int(result['status']), unit_status_2)

        # unsuccessful request incomplete command
        command = '|{}'.format(k_id)
        udp_request(command)

        # unsuccessful request unencrypted multi data with a key
        data = '{0} u {1} {2}|{3} u {4}'.format(self.unit1_id,
                                                unit_status_1, unit_value,
                                                self.unit2_id, unit_status_2)
        command = '|{}|{}'.format(k_id, data)
        udp_request(command)

        # unsuccessful request unencrypted multi data without a | (just first will be updated)
        data = '{0} u {1} {2} | {3} u {4}'.format(self.unit1_id,
                                                unit_status_1, unit_value,
                                                self.unit2_id, unit_status_2)
        command = '{}'.format(data)
        udp_request(command)

        # unsuccessful request encrypted data without a key
        data = '{0} u {1} {2}|{3} u {4}'.format(self.unit1_id,
                                                unit_status_1, unit_value,
                                                self.unit2_id, unit_status_2)
        k = eva.apikey.key_by_id(k_id)
        _k = base64.b64encode(hashlib.sha256(k.encode()).digest())
        ce = Fernet(_k)
        encr_data = ce.encrypt(data.encode()).decode()
        command = '|{}'.format(encr_data)
        udp_request(command)

        # unsuccessful request multiple encrypted commands without a starting |
        data = '{0} u {1} {2}|{3} u {4}'.format(self.unit1_id,
                                                unit_status_1, unit_value,
                                                self.unit2_id, unit_status_2)
        k = eva.apikey.key_by_id(k_id)
        _k = base64.b64encode(hashlib.sha256(k.encode()).digest())
        ce = Fernet(_k)
        encr_data = ce.encrypt(data.encode()).decode()
        command = '{}|{}|'.format(k_id, encr_data)
        udp_request(command)

        # unsuccessful request multiple encrypted commands without a starting | and key
        data = '{0} u {1} {2}|{3} u {4}'.format(self.unit1_id,
                                                unit_status_1, unit_value,
                                                self.unit2_id, unit_status_2)
        k = eva.apikey.key_by_id(k_id)
        _k = base64.b64encode(hashlib.sha256(k.encode()).digest())
        ce = Fernet(_k)
        encr_data = ce.encrypt(data.encode()).decode()
        command = '{}'.format( encr_data)
        udp_request(command)

def bash_state(item_id):
    bashCommand = 'bin/uc-cmd state {}'.format(item_id)
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    result = [i.lstrip() for i in output.decode().split('\n')]
    dict_result = {k:v for k,v in (i.split(' : ') for i in result if i != '')}
    return dict_result

def udp_request(command):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        pp = sock.sendto(bytes(command, 'utf-8'), ('127.0.0.1', 8881))

    # def test_echo(self):

    #     response = self.webapp_request('/echo', msg="hey there")
    #     self.assertEqual(response.status, '200 OK')
    #     self.assertEqual(response.body, [b"hey there"])

        # response = self.webapp_request('/echo', method='POST', msg="hey there")
        # self.assertEqual(response.status, '200 OK')
        # self.assertEqual(response.body, [b"hey there"])


if __name__ == "__main__":
    unittest.main()
