- clone tests in /tests directory
- pip3 install -r requirements.txt
- EVA ICS must be in simple layout and stopped

- SETUP_MODE=True option to enable setup mode
- UPD_TEST=True option and -a udp to run udp tests (uc server need to be launched)

i.e. run command when you are in /eva3 directory:
env EVA_DIR=`pwd` python3 -m "nose" tests/uc_tests/uc_test.py

useful nose options:
--pdb, Drop into debugger on failures or errors
-m=REGEX, Files, directories, function names, and class names that match this regular expression are considered tests.
-e=REGEX, Don�t run tests that match regular expression
-x, Stop running tests after the first error or failure
-s, Don�t capture stdout (any stdout output will be printed immediately)(use with pdb)
-P, Don�t make any changes to sys.path when loading tests
-a=ATTR, Run only tests that have attributes specified by ATTR
-d, Add detail to error output by attempting to evaluate failed asserts 