from io import StringIO
import unittest
import urllib
import cherrypy
import os
import copy
import sys
import json
from string import Formatter
from netaddr import IPNetwork

dir_lib = os.path.dirname(os.path.realpath(__file__)) + '/../../lib'
sys.path.append(dir_lib)

import eva.sysapi
import eva.core
import eva.traphandler
import eva.udpapi
import eva.notify
import eva.api
import eva.apikey
import eva.uc.controller
import eva.uc.ucapi
import eva.logs
import eva.runner
import eva.wsapi

local = cherrypy.lib.httputil.Host("127.0.0.1", 50000, "")
remote = cherrypy.lib.httputil.Host("127.0.0.1", 50001, "")
product_build = 2018073001
product_code = 'uc'

setup_mode = False
k = "k=eva&"

class EmptyNoneType(object):

    def __nonzero__(self):
        return False

    def __str__(self):
        return ''

    def __getattr__(self, name):
        return EmptyNone

    def __getitem__(self, idx):
        return EmptyNone
    

EmptyNone = EmptyNoneType()

class EmptyNoneFormatter(Formatter):

    def get_value(self, field_name, args, kwds):
        v = Formatter.get_value(self, field_name, args, kwds)
        if v is None:
            return EmptyNone
        return v
fmt = EmptyNoneFormatter()

def setUpModule():
    cherrypy.config.update({'environment': "test_suite"})
    cherrypy.server.unsubscribe()
    eva.core.init()
    eva.core.set_product(product_code, product_build)
    eva.core.product_name = 'EVA Universal Controller'
    cfg_test = eva.core.load(os.path.dirname(os.path.realpath(__file__)) + '/test_uc.ini',
                                             initial=True)
    eva.traphandler.update_config(cfg_test)
    eva.sysapi.update_config(cfg_test)
    eva.apikey.allows = ['cmd', 'lock', 'device']
    eva.apikey.load()
    eva.traphandler.start()
    eva.uc.controller.start()
    cherrypy.tree.mount(eva.sysapi.SysHTTP_API(), "/")
    cherrypy.engine.start()
setup_module = setUpModule

def tearDownModule():
    eva.uc.controller.stop()
    cherrypy.engine.exit()
teardown_module = tearDownModule

class BaseCherryPyTestCase(unittest.TestCase):
    key = k
    unit1_id = "test_unit1"
    unit2_id = "test_unit2"
    unit3_id = "test_unit3"
    group1 = "test_group1"
    group2 = "test_group2"
    group3 = "test_group3"
    groups = [group1, group2, group3]

    def webapp_request(self, path="/", method="GET", key_param=None):
        headers = [("Host", "127.0.0.1")]
        app = cherrypy.tree.apps['']
        request, response = app.get_serving(local, remote, "http", "HTTP/1.1")
        response = request.run(method, path, key_param, 'HTTP/1.1', headers, None)

        if response.status.startswith("500"):
            print(response.body)
            raise AssertionError("500 Unexpected error")
        return response

class TestSysAPI(BaseCherryPyTestCase):
    def setUp(self):
        global setup_mode, k
        if os.getenv('SETUP_MODE', False):            
            setup_mode = True
            k = None
            eva.core.setup_on()

    def tearDown(self):
        # from eva.apikey import keys, keys_by_id, _keys_loaded_from_ini
        # for i in copy.copy(keys_by_id):
        #     if i not in _keys_loaded_from_ini:
        #         del keys[keys_by_id[i].key]
        #         del keys_by_id[i]
        # eva.apikey.keys = {}
        # eva.apikey.keys_by_id = {}
        try:
            db = eva.core.get_db()
            c = db.cursor()
            try:
                c.execute("drop table users")
            except:
                pass
            try:
                c.execute("drop table apikeys")
            except:
                pass
            c.close()
            db.close()
        except:
            pass

    def create_unit(self, unit_id, group_id):
        response = self.webapp_request(path="/create_unit",
                                       key_param=fmt.format("{0}k={1}&i={2}&g={3}",
                                       self.key,
                                       unit_id, group_id))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

    def check_forbidden(self, path):
        if not setup_mode:
            response = self.webapp_request(path=path, key_param="k=wrong_key")
            self.assertEqual(response.status, "403 API Forbidden")

    def test_test(self):
        # wrong key
        self.check_forbidden("/test")

        response = self.webapp_request("/test",
                                       key_param=fmt.format("{0}", self.key))

        # check state

        self.assertEqual(response.status, "200 OK")

        response = json.loads(response.collapse_body())

        for i in ["debug", "polldelay", "product_build",
                  "product_code", "product_name", "result", "system",
                  "time", "uptime", "version", "acl"]:
            self.assertIn(i, response)
        if not setup_mode:
            self.assertEqual(response["acl"]["key_id"], "masterkey")
            self.assertEqual(response["acl"]["master"], True)

    @unittest.skipIf(os.getenv('SETUP_MODE'), "check of 404")
    def test_cmd(self):        
        # wrong key
        self.check_forbidden("/cmd")
        # test script completed        

        response = self.webapp_request(
                                       path="/cmd",
                                       key_param=fmt.format("{0}c={1}&a={2}&w={3}", 
                                       k, "test", 0, 3))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["args"], ["0"])
        self.assertEqual(response["cmd"], "test")
        self.assertEqual(response["err"], "some text to stderr\n")
        self.assertEqual(response["args"], ["0"])
        self.assertEqual(response["exitcode"], 0)
        self.assertIn("test script start\nparam 1: 0", response["out"])
        self.assertEqual(response["status"], "completed")
        self.assertEqual(response["timeout"], 5)

        # test script terminated
        response = self.webapp_request(path="/cmd",
                                       key_param=fmt.format("{0}c={1}&a={2}&w={3}&t={4}",
                                       self.key, "test", 0, 3, 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response["args"], ["0"])
        self.assertEqual(response["cmd"], "test")
        self.assertEqual(response["exitcode"], -15)
        self.assertEqual(response["status"], "terminated")
        self.assertEqual(response["timeout"], 1.0)

    def test_lock_token(self):
        # self.check_forbidden("/lock")

        response = self.webapp_request(path="/lock",
                                       key_param=fmt.format("{0}l={1}",
                                       self.key, "key1secret"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        response = self.webapp_request(path="/unlock",
                                       key_param=fmt.format("{0}l={1}",
                                       self.key, "key1secret"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

    def test_set_get_cvar(self):
        self.check_forbidden("/set_cvar")

        response = self.webapp_request(path="/set_cvar",
                                       key_param=fmt.format("{0}&i={1}&v={2}",
                                       self.key, "some_name", "some_value"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        response = self.webapp_request(path="/get_cvar",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, "some_name"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"some_name": "some_value"})
        
    def _test_global_save(self):
        self.check_forbidden("/save")

        # turn on global save
        eva.core.db_update = 0

        k_id1, k_id2 = 'test_key1', 'test_key2'
        hosts_allow = '192.168.1.0/8,192.168.2.0/8'
        hosts_assign = '192.168.1.1'
        items = '{},{}'.format(self.unit1_id, self.unit2_id)
        groups = '{},{}'.format(self.group1, self.group2)
        allow = 'cmd,lock'
        pvt_files = 'map.jpg,c/%23' # %23 escape for "#"" symbol that is reserved 
        rpvt_uris = '192.168.1.20/charts/%23'

        # create two apikeys, regenerate first one key and update second
        response = self.webapp_request(path="/add_api_key",
                                       key_param=fmt.format("{0}n={1}&hal={2}",
                                       self.key, k_id1, hosts_allow))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        key = response['key']

        response = self.webapp_request(path="/add_api_key",
                                       key_param=fmt.format("{0}n={1}&hal={2}",
                                       self.key, k_id2, hosts_allow))
        self.assertEqual(response.status, "200 OK")

        response = self.webapp_request(path="/modify_api_key",
                                       key_param=fmt.format("{0}n={1}&hal={2}\
                                        &s={3}&a={4}&g={5}&i={6}&has={7}&pvt={8}\
                                        &rpvt={9}",
                                       self.key, k_id2, hosts_allow, True, allow,
                                       groups, items, hosts_assign, pvt_files,
                                       rpvt_uris))
        self.assertEqual(response.status, "200 OK")

        response = self.webapp_request(path="/regenerate_api_key",
                                       key_param=fmt.format("{0}n={1}",
                                       self.key, k_id1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        # check apikeys aren`t saved to db
        db = eva.core.get_db()
        c = db.cursor()
        try:
            c.execute("select * from apikeys where k_id = ?", (k_id1))
        except Exception as e:
            self.assertEqual(e.args, ('no such table: apikeys',))
        db.commit()
        c.close()
        db.close()

        response = self.webapp_request(path="/save",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        # check apikeys are now saved to db
        db = eva.core.get_db()
        c = db.cursor()
        try:
            c.execute("select * from apikeys")
        except:
            pass
        rows = c.fetchall()
        db.commit()
        c.close()
        db.close()

        self.assertEqual(rows[0][0], k_id1)
        self.assertEqual(rows[1][0], k_id2)
        self.assertEqual(len(rows[1][1]), 64)
        self.assertNotEquals(rows[1][1], key)
        self.assertEqual(rows[1][2], 0)
        self.assertEqual(rows[1][3], 1)
        self.assertEqual(rows[1][4], items)
        self.assertEqual(rows[1][5], groups)
        self.assertEqual(rows[1][6], allow)
        self.assertEqual(rows[1][7], hosts_allow)
        self.assertEqual(rows[1][8], str(IPNetwork(hosts_assign)))
        self.assertEqual(rows[1][9], pvt_files.replace('%23', '#'))
        self.assertEqual(rows[1][10], rpvt_uris.replace('%23', '#'))

        # delete apikey
        self.check_forbidden('/delete_api_key')

        response = self.webapp_request(path="/delete_api_key",
                                       key_param=fmt.format("{0}n={1}",
                                       self.key, k_id1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})


        # check apikey is still present in db
        db = eva.core.get_db()
        c = db.cursor()
        try:
            c.execute("select * from apikeys where k_id = ?", (k_id1,))
        except:
            pass
        row = c.fetchone()
        db.commit()
        c.close()
        db.close()
        self.assertEqual(rows[0][0], k_id1)

        response = self.webapp_request(path="/save",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        # check apikey is now deleted from db
        db = eva.core.get_db()
        c = db.cursor()
        try:
            c.execute("select * from apikeys")
        except:
            pass
        rows = c.fetchall()
        db.commit()
        c.close()
        db.close()
        self.assertNotIn(k_id1, [i[0] for i in rows])
        self.assertIn(k_id2, [i[0] for i in rows])

    def test_users(self):
        self.check_forbidden("/list_keys")

        response = self.webapp_request(path="/list_keys",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(response[0], {'dynamic': False, 'key_id': 'masterkey', 'master': True})

        self.check_forbidden("/list_users")

        response = self.webapp_request(path="/list_users",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, [])

        self.check_forbidden("/create_user")

        response = self.webapp_request(path="/create_user",
                                       key_param=fmt.format("{0}u={1}&p={2}&a={3}",
                                       self.key, "some_name", "some_pass",
                                       "masterkey"))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        response = self.webapp_request(path="/list_users",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response[0], {'key': 'masterkey', 'user': 'some_name'})

        self.check_forbidden("/set_user_password")

        response = self.webapp_request(path="/set_user_password",
                                       key_param=fmt.format("{0}&u={1}&p={2}",
                                       self.key, "some_name", "new_pass"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        self.check_forbidden("/set_user_key")

        response = self.webapp_request(path="/set_user_key",
                                       key_param=fmt.format("{0}u={1}&a={2}",
                                       self.key, "some_name", "masterkey"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        self.check_forbidden("/destroy_user")

        response = self.webapp_request(path="/destroy_user",
                                       key_param=fmt.format("{0}u={1}",
                                       self.key, "some_name"))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        response = self.webapp_request(path="/list_users",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, [])

    def test_files(self):
        self.check_forbidden("/file_put")
        file_name = "test.json"
        file_path = os.path.dirname(os.path.realpath(__file__)) + \
                                    "/../../runtime/" + file_name
        f_body = json.dumps({"some_name": "some_value"})

        # delete saved before test files
        try:
            os.remove(file_path)
        except:
            pass

        response = self.webapp_request(path="/file_put",
                                       key_param=fmt.format("{0}i={1}&m={2}",
                                       self.key, file_name, f_body))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        # check file is locally saved and its permission
        self.assertEqual(os.access(file_path, os.F_OK), True)
        self.assertEqual(os.access(file_path, os.X_OK), False)

        self.check_forbidden("/file_get")

        response = self.webapp_request(path="/file_get",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, file_name))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(response["data"], '{"some_name": "some_value"}')
        self.assertEqual(response["file"], file_name)
        self.assertEqual(response["result"], "OK")

        self.check_forbidden("/file_set_exec")

        response = self.webapp_request(path="/file_set_exec",
                                       key_param=fmt.format("{0}i={1}&e={2}",
                                       self.key, file_name, 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(os.access(file_path, os.X_OK), True)

        response = self.webapp_request(path="/file_set_exec",
                                       key_param=fmt.format("{0}i={1}&e={2}",
                                       self.key, file_name, 0))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(os.access(file_path, os.X_OK), False)

        self.check_forbidden("/file_unlink")

        response = self.webapp_request(path="/file_unlink",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, file_name))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(os.access(file_path, os.F_OK), False)

    def test_log(self):
        self.check_forbidden("/log_rotate")

        response = self.webapp_request(path="/log_rotate",
                                       key_param=fmt.format("{0}", self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})


        for i in ["/log_debug", "/log_info", "/log_warning", "/log_error",
                  "/log_critical"]:
            self.check_forbidden(i)
            response = self.webapp_request(path=i,
                                           key_param=fmt.format("{0}m={1}",
                                           self.key, "log_message"))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {"result": "OK"})

        self.check_forbidden("/log_get")

        # check getting last 5 uc_log entries and filter them by level and time
        response = self.webapp_request(path="/log_get",
                                       key_param=fmt.format("{0}l={1}&t={2}&n={3}",
                                       self.key, 30, 50, 5))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(len(response), 5)
        for i in response:
            self.assertGreater(i["l"], 20)

        self.check_forbidden("/set_debug")

        response = self.webapp_request(path="/set_debug",
                                       key_param=fmt.format("{0}debug={1}",
                                       self.key, 1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

        response = self.webapp_request(path="/set_debug",
                                       key_param=fmt.format("{0}debug={1}",
                                       self.key, 0))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

    def test_apikey(self):

        k_id1, k_id2 = 'test_key1', 'test_key2'
        hosts_allow = '192.168.1.0/8,192.168.2.0/8'
        hosts_assign = '192.168.1.1'
        items = '{},{}'.format(self.unit1_id, self.unit2_id)
        groups = '{},{}'.format(self.group1, self.group2)
        allow = 'cmd,lock'
        pvt_files = 'map.jpg,c/%23' # %23 escape for "#"" symbol that is reserved 
        rpvt_uris = '192.168.1.20/charts/%23'

        props = {
        'hosts_allow': '192.168.1.0/8,192.168.2.0/8',
        'hosts_assign': '192.168.1.1',
        'items': '{},{}'.format(self.unit1_id, self.unit2_id),
        'groups': '{},{}'.format(self.group1, self.group2),
        'allow': 'cmd,lock',
        'pvt': 'map.jpg,c/%23', # %23 escape for "#"" symbol that is reserved 
        'rpvt': '192.168.1.20/charts/%23',
        'sysfunc': True}

        self.check_forbidden('/create_key')
        # try create key without required options
        response = self.webapp_request(path="/create_key",
                                       key_param=fmt.format("{0}",
                                       self.key))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "ERROR"})

        # create key with
        response = self.webapp_request(path="/create_key",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, k_id1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(response['id'], k_id1)
        self.assertEqual(response['hosts_allow'][0],"0.0.0.0/0")
        self.assertEqual(response['hosts_assign'], [])
        self.assertEqual(response['dynamic'], True)
        self.assertEqual(response['groups'], [])
        self.assertEqual(response['items'], [])
        self.assertEqual(len(response['key']), 64)
        self.assertEqual(response['master'], False)
        self.assertEqual(response['pvt'], [])
        self.assertEqual(response['rpvt'], [])
        self.assertEqual(response['sysfunc'], False)
        key = response['key']

        self.check_forbidden('/set_key_prop')
        # set key props
        for k, v in props.items():
            response = self.webapp_request(path="/set_key_prop",
                                           key_param=fmt.format("{0}i={1}&p={2}&v={3}",
                                           self.key, k_id1, k, v))
            self.assertEqual(response.status, "200 OK")
            response = json.loads(response.collapse_body())
            self.assertEqual(response, {"result": "OK"})

        self.check_forbidden('/list_key_props')
        # list key props
        response = self.webapp_request(path="/list_key_props",
                                       key_param=fmt.format("{0}i={1}",
                                        self.key, k_id1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())

        self.assertEqual(response['id'], k_id1)
        self.assertEqual('{},{}'.format(
            str(IPNetwork(response['hosts_allow'][0])),
            str(IPNetwork(response['hosts_allow'][1]))), hosts_allow)
        self.assertEqual(len(response['key']), 64)
        self.assertEqual(str(IPNetwork(response['hosts_assign'][0]).ip),
                                        hosts_assign)
        self.assertEqual(response['allow'], allow.split(','))
        self.assertEqual(response['groups'], groups.split(','))
        self.assertEqual(response['items'], items.split(',')) 
        self.assertEqual(response['sysfunc'], True)
        self.assertEqual(response['dynamic'], True)
        self.assertEqual(response['master'], False)
        self.assertEqual(response['pvt'],
                          pvt_files.replace('%23', '#').split(','))
        self.assertEqual(response['rpvt'],
                          rpvt_uris.replace('%23', '#').split(','))

        # set key bad host allowed option
        bad_hosts_allow = '=192=.168.2.0/8,192.168.3.0/8'

        response = self.webapp_request(path="/set_key_prop",
                                       key_param=fmt.format("{0}i={1}&p={2}&v={3}",
                                       self.key, k_id1, 'hal', bad_hosts_allow))

        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "ERROR"})

        # try to create apikey with name that exist
        response = self.webapp_request(path="/create_key",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, k_id1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "ERROR"})

        # try to change masterkey
        response = self.webapp_request(path="/set_key_prop",
                                       key_param=fmt.format("{0}i={1}&p={2}&v={3}",
                                       self.key, 'masterkey', 'hal', hosts_allow))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "ERROR"})

        # test regenerate key
        self.check_forbidden('/regenerate_key')

        response = self.webapp_request(path="/regenerate_key",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, k_id1))
        self.assertEqual(response.status, "200 OK")

        response = json.loads(response.collapse_body())
        self.assertEqual(len(response['key']), 64)
        self.assertNotEqual(response, key)

        # test delete apikey
        self.check_forbidden('/destroy_key')

        response = self.webapp_request(path="/destroy_key",
                                       key_param=fmt.format("{0}i={1}",
                                       self.key, k_id1))
        self.assertEqual(response.status, "200 OK")
        response = json.loads(response.collapse_body())
        self.assertEqual(response, {"result": "OK"})

if __name__ == "__main__":
    unittest.main()